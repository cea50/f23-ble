#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/sys/printk.h>
#include <zephyr/logging/log.h>
#include <zephyr/smf.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/pwm.h>
#include <nrfx_power.h> // NOTE: This is not a Zephyr library!!  It is a Nordic NRFX library.
#include "pressure.h"
#include <nrfx_power.h> // for VBUS - NOTE: This is not a Zephyr library!!  It is a Nordic NRFX library.
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/hci.h> // host controller interface
#include <zephyr/bluetooth/services/bas.h>  // battery service GATT
#include <zephyr/settings/settings.h>

LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

#define OVERSAMPLE 10  // number of samples to average together
#define HEARTBEAT_INTERVAL_MS 1000
#define ERROR_INTERVAL_MS 1000
#define MAX_VOLTAGE 3.3
#define V_TO_MV 0.001
#define MAX_BRIGHTNESS 1
#define MEASUREMENT_DELAY_MS 1000
#define DATA_ARRAY 1000
#define PERIOD_ARRAY 10

/* Define macros for UUIDs of the Remote Service
   Project ID: 001 (3rd entry)
   MFG ID = 0x01FF (4th entry)
*/
#define BT_UUID_REMOTE_SERV_VAL \
        BT_UUID_128_ENCODE(0xe9ea0000, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_SERVICE 			BT_UUID_DECLARE_128(BT_UUID_REMOTE_SERV_VAL)

/* UUID of the Data Characteristic */
#define BT_UUID_REMOTE_DATA_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0001, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_DATA_CHRC 		BT_UUID_DECLARE_128(BT_UUID_REMOTE_DATA_CHRC_VAL)

/* UUID of the Message Characteristic */
#define BT_UUID_REMOTE_MESSAGE_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0002, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_MESSAGE_CHRC 	BT_UUID_DECLARE_128(BT_UUID_REMOTE_MESSAGE_CHRC_VAL) 

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME)-1)
#define BLE_DATA_POINTS 600 // limited by MTU

int interval = 100;
int interval2 = 2000;
int32_t val_mv;
int32_t val_mv1;
int32_t val_mv2;
static int32_t atm_pressure_kPa;
int period;
int i = 0;
int j = 0;
int err;
int error_type; //if error_type is 1 its a VBUS error if its 2 is a pressure sensor error 
float battery_percent;
float battery_level;
int normalized_battery_level;
float raw_data_array1[DATA_ARRAY]={};
float max_data_array1[PERIOD_ARRAY]={};
float min_data_array1[PERIOD_ARRAY]={};
float raw_data_array2[DATA_ARRAY]={};
float max_data_array2[PERIOD_ARRAY]={};
float min_data_array2[PERIOD_ARRAY]={};
float led_data_array[3]={};

enum bluetooth_outputs {led1_brightness, led2_brightness, pressure_reading};
int16_t bluetooth_outputs[3];

// honeywell_mpr is a default device in the Zephyr ecosystem
const struct device *const pressure_in = DEVICE_DT_GET_ONE(honeywell_mpr);

#define LED0_NODE DT_ALIAS(led1)
static const struct gpio_dt_spec led1 = GPIO_DT_SPEC_GET(LED0_NODE,gpios);
#define LED1_NODE DT_ALIAS(led2)
static const struct gpio_dt_spec led2 = GPIO_DT_SPEC_GET(LED1_NODE,gpios);
#define LED2_NODE DT_ALIAS(led3)
static const struct gpio_dt_spec led3 = GPIO_DT_SPEC_GET(LED2_NODE,gpios); //error LED
#define LED3_NODE DT_ALIAS(heartbeat)
static const struct gpio_dt_spec heartbeat = GPIO_DT_SPEC_GET(LED3_NODE,gpios);
#define BUTTON0_NODE DT_ALIAS(button1)
static const struct gpio_dt_spec button1 = GPIO_DT_SPEC_GET(BUTTON0_NODE,gpios);
#define BUTTON1_NODE DT_ALIAS(button2)
static const struct gpio_dt_spec button2 = GPIO_DT_SPEC_GET(BUTTON1_NODE,gpios);

#define ADC_DT_SPEC_GET_BY_ALIAS(vadc)                    \
{                                                            \
   .dev = DEVICE_DT_GET(DT_PARENT(DT_ALIAS(vadc))),      \
   .channel_id = DT_REG_ADDR(DT_ALIAS(vadc)),            \
   ADC_CHANNEL_CFG_FROM_DT_NODE(DT_ALIAS(vadc))          \
}                                                            \

//**********************************************************************************************
//BLUETOOTH #TRASH
// enumeration to keep track of the state of the notifications
enum bt_data_notifications_enabled {
    BT_DATA_NOTIFICATIONS_ENABLED,
    BT_DATA_NOTIFICATIONS_DISABLED,
};
enum bt_data_notifications_enabled notifications_enabled;

// declare struct of the BLE remove service callbacks
struct bt_remote_srv_cb {
    void (*notif_changed)(enum bt_data_notifications_enabled status);
    void (*data_rx)(struct bt_conn *conn, const uint8_t *const data, uint16_t len);
}; 

struct bt_remote_srv_cb remote_service_callbacks;

// blocking thread semaphore to wait for BLE to initialize
static K_SEM_DEFINE(bt_init_ok, 1, 1);

/* Advertising data */
static const struct bt_data ad[] = {
    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
    BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN)
};

/* Scan response data */
static const struct bt_data sd[] = {
    BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_REMOTE_SERV_VAL), 
};
//**********************************************************************************************

//Get the ADC io-channel information from device tree
static const struct adc_dt_spec adc_vadc = ADC_DT_SPEC_GET_BY_ALIAS(vadc);
static const struct adc_dt_spec adc_vadc1 = ADC_DT_SPEC_GET_BY_ALIAS(vadc1);
static const struct adc_dt_spec adc_vadc2 = ADC_DT_SPEC_GET_BY_ALIAS(vadc2);

//Define structs based on DT alias
static const struct pwm_dt_spec led1_pwm = PWM_DT_SPEC_GET(DT_ALIAS(pwm1));
static const struct pwm_dt_spec led2_pwm = PWM_DT_SPEC_GET(DT_ALIAS(pwm2));

// defining heartbeat and LED timer handler functions
void heartbeat_timer_function(struct k_timer *heartbeat_timer);
void error_timer_function(struct k_timer *error_timer);
void led_timer_start(struct k_timer * led_timer);
void led_timer_stop(struct k_timer * led_timer);
void button1_callback(const struct device *dev, struct gpio_callback*cb, uint32_t pins);
void button2_callback(const struct device *dev, struct gpio_callback*cb, uint32_t pins);

// defining work handlers
void heartbeat_timer_work_handler(struct k_work *heartbeat_timer_work);
void error_timer_work_handler(struct k_work *error_timer_work);
void led_start_work_handler(struct k_work *led_timer_start_work);
void led_stop_work_handler(struct k_work *led_timer_stop_work);
void button1_callback_work_handler(struct k_work *button1_callback_work);
void button2_callback_work_handler(struct k_work *button2_callback_work);

/* Function Declarations Bluetooth*/
static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset);
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value);
static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags);
void bluetooth_set_battery_level(int level, int nominal_batt_mv);
uint8_t bluetooth_get_battery_level(void);

/* Function Declarations more Bluetooth*/
static struct bt_conn *current_conn;
void on_connected(struct bt_conn *conn, uint8_t ret);
void on_disconnected(struct bt_conn *conn, uint8_t reason);
void on_notif_changed(enum bt_data_notifications_enabled status);
void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len);

// Defining how long the timers start and stop for
K_TIMER_DEFINE(heartbeat_timer,heartbeat_timer_function,NULL);
K_TIMER_DEFINE(error_timer,error_timer_function,NULL);
K_TIMER_DEFINE(led_timer,led_timer_start,led_timer_stop);

// Defining which work handler corresponds to which work function
K_WORK_DEFINE(heartbeat_timer_work, heartbeat_timer_work_handler);
K_WORK_DEFINE(error_timer_work, error_timer_work_handler);
K_WORK_DEFINE(led_timer_start_work, led_start_work_handler);
K_WORK_DEFINE(led_timer_stop_work, led_stop_work_handler);
K_WORK_DEFINE(button1_callback_work, button1_callback_work_handler);
K_WORK_DEFINE(button2_callback_work, button2_callback_work_handler);

//defining the different substates within each state
static int init_entry(void *o);
static int init_run(void *o);
static void idle_entry(void *o);
static void idle_run(void *o);
static void idle_exit(void *o);
static void action_entry(void *o);
static void action_run(void *o);
static void action_exit(void *o);
static void error_entry(void *o);
static void error_run(void *o);
static void error_exit(void *o);

//creating the different states and populating state table
enum device_states {initstate, idlestate, actionstate,errorstate};
static const struct smf_state device_states[]={
[initstate] = SMF_CREATE_STATE(init_entry, init_run, NULL),
[idlestate] = SMF_CREATE_STATE(idle_entry, idle_run, idle_exit ),
[actionstate] = SMF_CREATE_STATE(action_entry, action_run, action_exit),
[errorstate] = SMF_CREATE_STATE(error_entry, error_run, error_exit),
};

/* User defined object */
struct s_object {
        /* This must be first */
        struct smf_ctx ctx;
        /* Other state specific data add here... */
} s_obj;

//*****************************************************************************************
//MORE BLUETOOTH....

/* BLE Callback Functions */
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value) {
    bool notif_enabled = (value == BT_GATT_CCC_NOTIFY);
    LOG_INF("Notifications: %s", notif_enabled? "enabled":"disabled");

    notifications_enabled = notif_enabled? BT_DATA_NOTIFICATIONS_ENABLED:BT_DATA_NOTIFICATIONS_DISABLED;

    if (remote_service_callbacks.notif_changed) {
        remote_service_callbacks.notif_changed(notifications_enabled);
    }
}

static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset) {
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &bluetooth_outputs, sizeof(bluetooth_outputs));
}

static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags) {
    LOG_INF("Received data, handle %d, conn %p", attr->handle, (void *)conn);
    
    if (remote_service_callbacks.data_rx) {
        remote_service_callbacks.data_rx(conn, buf, len);
    }
    return len;
}

void on_sent(struct bt_conn *conn, void *user_data) {
    ARG_UNUSED(user_data);
    LOG_INF("Notification sent on connection %p", (void *)conn);
}

void bt_ready(int ret) {
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
    }

    // release the thread once initialized
    k_sem_give(&bt_init_ok);
}

/* Setup BLE Services */
BT_GATT_SERVICE_DEFINE(remote_srv,
BT_GATT_PRIMARY_SERVICE(BT_UUID_REMOTE_SERVICE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_DATA_CHRC,
                BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                BT_GATT_PERM_READ,
                read_data_cb, NULL, NULL),
BT_GATT_CCC(data_ccc_cfg_changed_cb, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_MESSAGE_CHRC,
                BT_GATT_CHRC_WRITE_WITHOUT_RESP,
                BT_GATT_PERM_WRITE,
                NULL, on_write, NULL),
);

int send_data_notification(struct bt_conn *conn, uint8_t *value, uint16_t length) {
    int ret = 0;

    struct bt_gatt_notify_params params = {0};
    const struct bt_gatt_attr *attr = &remote_srv.attrs[2];

    params.attr = attr;
    params.data = &value;
    params.len = length;
    params.func = on_sent;

    ret = bt_gatt_notify_cb(conn, &params);

    return ret;
}

/* Initialize the BLE Connection */
int bluetooth_init(struct bt_conn_cb *bt_cb, struct bt_remote_srv_cb *remote_cb) {
    LOG_INF("Initializing Bluetooth");

    if ((bt_cb == NULL) | (remote_cb == NULL)) {
        return -NRFX_ERROR_NULL;
    }
    bt_conn_cb_register(bt_cb);
    remote_service_callbacks.notif_changed = remote_cb->notif_changed;
    remote_service_callbacks.data_rx = remote_cb->data_rx;

    int ret = bt_enable(bt_ready);
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
        return ret;
    }

        // hold the thread until Bluetooth is initialized
	k_sem_take(&bt_init_ok, K_FOREVER);

	if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
		settings_load();
	}

	ret = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), sd, ARRAY_SIZE(sd));
	if (ret) {
		LOG_ERR("Could not start advertising (ret = %d)", ret);
		return ret;
	}
	return ret;
}

/* Setup Callbacks */
struct bt_conn_cb bluetooth_callbacks = {
    .connected = on_connected,
    .disconnected = on_disconnected,
};

struct bt_remote_srv_cb remote_service_callbacks = {
    .notif_changed = on_notif_changed,
    .data_rx = on_data_rx,
};

void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len) {
    uint8_t temp_str[len+1];
    memcpy(temp_str, data, len);
    temp_str[len] = 0x00; // manually append NULL character at the end

    LOG_INF("BT received data on conn %p. Len: %d", (void *)conn, len);
    LOG_INF("Data: %s", temp_str);
}

void on_connected(struct bt_conn *conn, uint8_t ret) {
    if (ret) { LOG_ERR("Connection error: %d", ret); }
    LOG_INF("BT connected");
    current_conn = bt_conn_ref(conn);
}

void on_disconnected(struct bt_conn *conn, uint8_t reason) {
    LOG_INF("BT disconnected (reason: %d)", reason);
    if (current_conn) {
        bt_conn_unref(current_conn);
        current_conn = NULL;
    }
}

void on_notif_changed(enum bt_data_notifications_enabled status) {
    if (status == BT_DATA_NOTIFICATIONS_ENABLED) {
        LOG_INF("BT notifications enabled");
    }
    else {
        LOG_INF("BT notifications disabled");
    }
}
//****************************************************************************************
//heartbeat timer function work handler
void heartbeat_timer_function(struct k_timer *heartbeat_timer){
k_work_submit(&heartbeat_timer_work);
}
void heartbeat_timer_work_handler(struct k_work*heartbeat_timer_work){
gpio_pin_toggle_dt(&heartbeat);
}

//Error timer function work handler
void error_timer_function(struct k_timer *error_timer){
k_work_submit(&error_timer_work);
}
void error_timer_work_handler(struct k_work*error_timer_work){
if (error_type == 1){
        gpio_pin_toggle_dt(&led3);
}
else if (error_type ==2){
        gpio_pin_set_dt(&led3,0);       
}
}

//LED timer has been started
void led_timer_start(struct k_timer *led_timer){
        k_work_submit(&led_timer_start_work);}  
void led_start_work_handler(struct k_work*led_timer_start_work){
        //calculations for LED1 and LED 2 
        if (i < DATA_ARRAY){
                raw_data_array1[i++] = val_mv1;
                raw_data_array2[i++] = val_mv2;
        }
        else if(i = DATA_ARRAY) {
                LOG_INF("Time has run out for one run");
                k_timer_stop(&led_timer);
        }
        LOG_INF("You are in the start work handler");
}

//LED timer has finished
void led_timer_stop(struct k_timer *led_timer){
        k_work_submit(&led_timer_stop_work);}
void led_stop_work_handler(struct k_work*led_timer_stop_work){
        // #movetoktimerstop; #winning
        float min_value1;
        float max_value1;
        float min_value2;
        float max_value2;

        if (period < PERIOD_ARRAY){
                // Iterate through the array to find min and max
                period++;
                min_value1 = raw_data_array1[i];
                max_value1 = raw_data_array1[i];
                min_value2 = raw_data_array2[i];
                max_value2 = raw_data_array2[i];

                for (int j = 0; j< DATA_ARRAY; j++){
                        if (raw_data_array1[j] < min_value1) {
                                min_value1 = raw_data_array1[j];
                        }       
                        else if (raw_data_array1[j] > max_value1) {
                                max_value1 = raw_data_array1[j];
                        }
                        if (raw_data_array2[j] < min_value2) {
                                min_value1 = raw_data_array1[j];
                        }       
                        else if (raw_data_array2[j] > max_value2) {
                                max_value2 = raw_data_array2[j];
                        }
                }
                max_data_array1[j++] = max_value1;
		min_data_array1[j++] = min_value1;
                max_data_array2[j++] = max_value2;
		min_data_array2[j++] = min_value2;

                i = 0;
		k_timer_start(&led_timer, K_MSEC(100), K_MSEC(100));
        }

        else if (period = PERIOD_ARRAY) {
                //we calculate average max and min values
		max_data_array1[i++] = max_value1;
		min_data_array1[i++] = min_value1;
                max_data_array2[i++] = max_value2;
		min_data_array2[i++] = min_value2;
                float sum_max1 = 0;
                float sum_min1 = 0;
                float sum_max2 = 0;
                float sum_min2 = 0;
                for (int i = 0; i < DATA_ARRAY; i++){
                        sum_max1 += max_data_array1[i];
                        sum_max2 += max_data_array2[i];
                }
                for (int i = 0; i < DATA_ARRAY; i++){    
                        sum_min1 += min_data_array1[i];
                        sum_min2 += min_data_array2[i];
                }
                float average_max1;
                float average_max2;
                average_max1 = sum_max1 / (DATA_ARRAY);
                average_max2 = sum_max2 / (DATA_ARRAY);
                LOG_INF("max average1 %f", average_max1);

                float average_min1;
                float average_min2;
                average_min1 = sum_min1 / (DATA_ARRAY);
                average_min2 = sum_min2 / (DATA_ARRAY);
                LOG_INF("min average %f", average_min1);

                float led1_vpp = average_max1 - average_min1;
                float led2_vpp = average_max2 - average_min2;
        
                float led1_brightness_val = ((1000000*(led1_vpp+5)))/(50+5);
                float led1_brightness_percentage = led1_brightness_val/10000;
                float led2_brightness_val = ((1000000*(led2_vpp+10)))/(150+10);
                float led2_brightness_percentage = led2_brightness_val/10000;
                LOG_INF("Led 1 Brightness: %f", led1_brightness_val);
                LOG_INF("Brightness Percentage: %f", led1_brightness_percentage);
                pwm_set_pulse_dt(&led1_pwm, (int)led1_brightness_val);
                pwm_set_pulse_dt(&led2_pwm, (int)led2_brightness_val);

                bluetooth_outputs[led1_brightness] = (int)led1_brightness_val;
                bluetooth_outputs[led2_brightness] = (int)led2_brightness_val;
                smf_set_state(SMF_CTX(&s_obj), &device_states[idlestate]);
	}
        LOG_INF("You are going to the idle state");
}

//button1 
static struct gpio_callback button1_cb;
void button1_callback(const struct device *dev, struct gpio_callback*cb, uint32_t pins){
        k_work_submit(&button1_callback_work);
}
void button1_callback_work_handler(struct k_work*button1_callback_work){
        k_timer_start(&led_timer, K_MSEC(interval), K_MSEC(interval));
        period = 0;
        smf_set_state(SMF_CTX(&s_obj), &device_states[actionstate]);
}

//button 2
static struct gpio_callback button2_cb;
void button2_callback(const struct device *dev, struct gpio_callback*cb, uint32_t pins){
        k_work_submit(&button2_callback_work);
}
void button2_callback_work_handler(struct k_work*button2_callback_work){
        battery_percent = ((100*val_mv)/3600);
        LOG_INF("Battery_percent: %f",  battery_percent);

        int battery_err = bt_bas_set_battery_level((int)battery_level);
        if (battery_err) {
                LOG_ERR("BAS set error (err = %d)", battery_err);
        }

        battery_level =  bt_bas_get_battery_level();
        err = send_data_notification(current_conn, bluetooth_outputs, 3);
         if (err) {
                 LOG_ERR("Could not send BT notification (err: %d)", err);
         }
         else {
                 LOG_INF("BT data transmitted.");
         }

        //updates battery information
        err = bt_bas_set_battery_level((int)normalized_battery_level);
        if (err) {
                 LOG_ERR("BAS set error (err = %d)", err);
        }
}
//********************************************************************************************

static int init_entry(void *o){
        int ret;
        ret = gpio_pin_configure_dt(&heartbeat, GPIO_OUTPUT_ACTIVE);
        if (ret<0){
                return ret;
        }
        ret = gpio_pin_configure_dt(&led1, GPIO_OUTPUT_ACTIVE);
        if (ret<0){
                return ret;
        }
        ret = gpio_pin_configure_dt(&led2, GPIO_OUTPUT_ACTIVE);
        if (ret<0){
                return ret;
        }
        ret = gpio_pin_configure_dt(&led3, GPIO_OUTPUT_ACTIVE);
        if (ret<0){
                return ret;
        }
        ret = gpio_pin_configure_dt(&button1, GPIO_INPUT);
        if (ret<0){
                return ret;
        }

        ret = gpio_pin_configure_dt(&button2, GPIO_INPUT);
        if (ret<0){
                return ret;
        }

        /* Check that the ADC interface is ready */
        if (!device_is_ready(adc_vadc.dev)) {
                LOG_ERR("ADC controller device(s) not ready");
                return -1;
        }

        //Check that the PWM controller is ready
        if (!device_is_ready(led1_pwm.dev))  {
                LOG_ERR("PWM1 device %s is not ready.", led1_pwm.dev->name);
                return -1;
        }
        if (!device_is_ready(led2_pwm.dev))  {
                LOG_ERR("PWM2 device %s is not ready.", led2_pwm.dev->name);
                return -1;
        }

        //check that Pressure Sensor is ready
        if (!device_is_ready(pressure_in)) {
		        LOG_ERR("MPR pressure sensor %s is not ready", pressure_in->name);
		        return;
	    }
        else {
                LOG_INF("MPR pressure sensor %s is ready", pressure_in->name);
        }

        // set the PWM duty cycle (pulse length)
                err = pwm_set_pulse_dt(&led1_pwm, led1_pwm.period); // 100% duty cycle
                if (err) {
                LOG_ERR("Could not set led1_pwm driver 1 (PWM0)");
        }
         // set the PWM duty cycle (pulse length)
                err = pwm_set_pulse_dt(&led2_pwm, led2_pwm.period); // 100% duty cycle
                if (err) {
                LOG_ERR("Could not set led2_pwm driver 1 (PWM0)");
        }

        /* Configure the 1st ADC channel */
        err = adc_channel_setup_dt(&adc_vadc);
        if (err < 0) {
                LOG_ERR("Could not setup ADC channel (%d)", err);
                return err;
        }

         if (!device_is_ready(adc_vadc.dev)) {
                LOG_ERR("ADC controller device(s) not ready");
                return -1;
        }

        /* Configure the 2nd ADC channel */
        err = adc_channel_setup_dt(&adc_vadc1);
        if (err < 0) {
                LOG_ERR("Could not setup ADC channel (%d)", err);
                return err;
                }
        if (!device_is_ready(adc_vadc1.dev)) {
                LOG_ERR("ADC controller device(s) not ready");
                return -1;
        }

        /* Configure the 3rd ADC channel */
        err = adc_channel_setup_dt(&adc_vadc2);
        if (err < 0) {
                LOG_ERR("Could not setup ADC channel (%d)", err);
                return err;
        }
       if (!device_is_ready(adc_vadc2.dev)) {
                LOG_ERR("ADC controller device(s) not ready");
                return -1;
        }

        //  /* Initialize Bluetooth */
         //int err = bluetooth_init(&bluetooth_callbacks, &remote_service_callbacks);
         err = bluetooth_init(&bluetooth_callbacks, &remote_service_callbacks);
         if (err) {
                 LOG_ERR("BT init failed (err = %d)", err);
         }

        //k_timer_start(&led_timer, K_MSEC(interval), K_MSEC(interval));
}

static int init_run(void *o){
        //doing this because the timer starts on
        //k_timer_stop(&led_timer);
        //start heartbeat interval and go to idle state..... Change this to action
        k_timer_start(&heartbeat_timer, K_MSEC(HEARTBEAT_INTERVAL_MS), K_MSEC(HEARTBEAT_INTERVAL_MS));
        smf_set_state(SMF_CTX(&s_obj), &device_states[idlestate]); 
}

static int init_exit(void *o){
        //Make sure LEDs start off
        gpio_pin_set_dt(&led1, 0);
	gpio_pin_set_dt(&led2, 0);
	gpio_pin_set_dt(&led3,0);
        LOG_INF("You are Exiting Init Entry");
}

static void idle_entry(void *o){ 
        int ret;
        ret = gpio_pin_interrupt_configure_dt(&button1, GPIO_INT_EDGE_TO_ACTIVE);
        gpio_init_callback(&button1_cb, button1_callback, BIT(button1.pin));
        gpio_add_callback(button1.port, &button1_cb);

        ret = gpio_pin_interrupt_configure_dt(&button2, GPIO_INT_EDGE_TO_ACTIVE);
        gpio_init_callback(&button2_cb, button2_callback, BIT(button2.pin));
        gpio_add_callback(button2.port, &button2_cb);
        LOG_INF("You are in Idle Run");
}

static void idle_run(void *o){
        LOG_INF("You are in Idle Run");
        //checking if VBUS is detected
        bool usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
        if (usbregstatus == true) {
            //VBUS DETECTED
            error_type =1;
            smf_set_state(SMF_CTX(&s_obj), &device_states[errorstate]);
        } 

        //check pressure stuff
        float atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);
	k_msleep(MEASUREMENT_DELAY_MS);
	if (atm_pressure_kPa <= 0){
                error_type =2;
                smf_set_state(SMF_CTX(&s_obj), &device_states[errorstate]);
                bluetooth_outputs[pressure_reading] = atm_pressure_kPa;
        } 
}

static void idle_exit(void *o){
        LOG_INF("A button has been pressed");
}

static void action_entry(void *o){
        LOG_INF("You are in Action Entry");
        k_timer_start(&led_timer, K_MSEC(interval), K_MSEC(interval));
        //measurement delay
        //k_msleep(1000);
}

static void action_run(void *o){
        LOG_INF("You are in Action Run");

        // read the resultant data from the buffer
        // ADC buffer will yield a 16 but intender, but ADC is only 8-bit
        // must convert the ADC buffer data to actual voltages by scaling the ADC buffer data
        // by the reference voltage and the gain factor
        int ret;
        int ret1;
        int ret2;
        int16_t buf;
        int16_t buf1;
        int16_t buf2;

        struct adc_sequence sequence = {
        .buffer = &buf,  //ADC will store its data in the memory location pointed to by &buf
        .buffer_size = sizeof(buf), // bytes
        };
        struct adc_sequence sequence1 = {
        .buffer = &buf1, 
        .buffer_size = sizeof(buf1), 
        };
        struct adc_sequence sequence2 = {
        .buffer = &buf2, 
        .buffer_size = sizeof(buf2), 
        };

        //prints a message indicating the measurement is being taken on an ADC channel
        LOG_INF("Measuring %s (channel %d)... ", adc_vadc.dev->name, adc_vadc.channel_id);
        LOG_INF("Measuring %s (channel %d)... ", adc_vadc1.dev->name, adc_vadc1.channel_id);
        LOG_INF("Measuring %s (channel %d)... ", adc_vadc2.dev->name, adc_vadc2.channel_id);
        (void)adc_sequence_init_dt(&adc_vadc, &sequence);
        (void)adc_sequence_init_dt(&adc_vadc1, &sequence1);
        (void)adc_sequence_init_dt(&adc_vadc2, &sequence2);
        
        //reading the ADC data using adc_read function.. result of this is stored in ret
        ret = adc_read(adc_vadc.dev, &sequence);
                if (ret < 0) {
                LOG_ERR("Could not read (%d)", ret);
        } 
                else {
                //LOG_DBG("Raw ADC Buffer: %d", buf);
        }
        ret1 = adc_read(adc_vadc1.dev, &sequence1);
                if (ret1 < 0) {
                LOG_ERR("Could not read (%d)", ret1);
        } 
                else {
                //LOG_DBG("Raw ADC Buffer1: %d", buf1);
        }
        ret2 = adc_read(adc_vadc2.dev, &sequence2);
                if (ret2 < 0) {
                LOG_ERR("Could not read (%d)", ret2);
        } 
                else {
                //LOG_DBG("Raw ADC Buffer1: %d", buf2);
        }

        //converting the raw ADC data from the buf into millivolts using the function adc_raw_to_millivolts_dt
        val_mv = buf;
        ret = adc_raw_to_millivolts_dt(&adc_vadc, &val_mv); // remember that the vadc struct containts all the DT parameters
        if (ret < 0) {
                LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
        } 
        val_mv1 = buf1;
        ret1 = adc_raw_to_millivolts_dt(&adc_vadc1, &val_mv1);
        if (ret1 < 0) {
                LOG_ERR("Buffer1 cannot be converted to mV; returning raw buffer value.");
        } 
        val_mv2 = buf2;
        ret2 = adc_raw_to_millivolts_dt(&adc_vadc2, &val_mv2);
        if (ret2 < 0) {
               LOG_ERR("Buffer1 cannot be converted to mV; returning raw buffer value.");
        } 
        
        //calculating pressor 
        atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);

}

static void action_exit(void *o){
        LOG_INF("you are leaving action state");
        //k_timer_stop(&led_timer);
}

static void error_entry(void *o){
        LOG_INF("You are in entering Error State");
        gpio_pin_interrupt_configure_dt(&button1, GPIO_INT_DISABLE);
        gpio_pin_interrupt_configure_dt(&button2, GPIO_INT_DISABLE);
        pwm_set_pulse_dt(&led1_pwm, 0);
        pwm_set_pulse_dt(&led2_pwm, 0);
        k_timer_start(&error_timer, K_MSEC(ERROR_INTERVAL_MS), K_MSEC(ERROR_INTERVAL_MS));
}

static void error_run(void *o){
        LOG_INF("You are in Error Run");

        //Check to see if BUS is plugged in
        bool usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
        k_msleep(MEASUREMENT_DELAY_MS);
        if (usbregstatus == false) {
            smf_set_state(SMF_CTX(&s_obj), &device_states[idlestate]);
        }

        //checking pressure
        float atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);
	k_msleep(MEASUREMENT_DELAY_MS);
	if (atm_pressure_kPa > 0){
                smf_set_state(SMF_CTX(&s_obj), &device_states[idlestate]);
        } 
}

static void error_exit(void *o){
        LOG_INF("you are leaving Error state");
        k_timer_stop(&error_timer);
        gpio_pin_configure_dt(&led3,0);
}

void main(void){
        
        int32_t ret;

        //setting init as the initial state
        smf_set_initial(SMF_CTX(&s_obj), &device_states[initstate]);

        //Run the state machine
        while(1) {
                /* State machine terminates if a non-zero value is returned */
                ret = smf_run_state(SMF_CTX(&s_obj));
                if (ret) {
                        /* handle return code and terminate state machine */
                        break;
                }
                k_msleep(1000);
        }
}