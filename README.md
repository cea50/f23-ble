# BME590L F23-BLE Lab

Consult the [Zephyr-BLE](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-ble/-/blob/main/lab/Zephyr-BLE-Lab.md?ref_type=heads) lab for all details.  Some reminders:
* You should `Fork` this repository (not `clone` it!).
* Add Dr. Palmeri (`mlp6`) as a Maintainer.
* If you choose to work with a partner, add them as a Maintainer too.
* Each person should work on their own branches and merge them into `main` when "done".
* Submit a `Merge Request` to this repository when ready to submit everything.
  + The title of the merge request should be: "Draft: FullName1 [FullName2]"
  + Assign the merge request to Dr. Palmeri (`mlp6`).
